<?php

use Illuminate\Http\Request;

Route::post('register', 'Api\RegisterController@register');
Route::post('login', 'Api\RegisterController@login');

Route::middleware('auth:api')->group(function () {

    Route::post('get-users', 'Api\RegisterController@getOthersUsers');
    Route::post('send-message','Api\MessageController@sendMessage');
    Route::post('get-message','Api\MessageController@getMessage');

});

