Установка.

1. Клонировать данный репозиторий.
2. Перейти в папку проекта и запустить комманду composer install.
3. Скопировать .env.example в .env и установить параметры соединения с базой данных.
4. Сгенерировать ключ php artisan key:generate.
5. Запустить миграции БД php artisan migrate.
6. Для получения токенов безопасности, нужно запустить команду php artisan passport:install. Полученный токен и id клиента необходимо сохранить для дальнейшего использования.



Используемые роуты прописаны в routes/api.php


Регистрация пользователя

Url: http://127.0.0.1:8000/api/register
Method: post
Заголовки:
Accept:application/json
Authorization:Bearer <client_secret>

Параметры:
name - имя пользователя
email - валидный e-mail пользователя
password - пароль пользователя
c_password - подтверждение пароля

https://bitbucket.org/druos4/apis/raw/1f53062aea0863317523dc1e6cceb3b6bcae4119/public/images/reg.jpg



Авторизация

Url: http://localhost:8000/api/login
Method: post
Заголовки:
Accept:application/json
Authorization:Bearer <client_secret>
remember_me:true

Параметры:
email - e-mail пользователя
password - пароль пользователя

Ответ:
access_token - токен, который в дальнейшем используется для запросов по api

https://bitbucket.org/druos4/apis/raw/1f53062aea0863317523dc1e6cceb3b6bcae4119/public/images/login.jpg


Получение списков пользователей

Url: http://localhost:8000/api/get-users
Method: post
Заголовки:
Accept:application/json
Authorization:Bearer <access_token>

https://bitbucket.org/druos4/apis/raw/1f53062aea0863317523dc1e6cceb3b6bcae4119/public/images/users.jpg


Отправить сообщение

Url: http://localhost:8000/api/send-message
Method: post
Заголовки:
Accept:application/json
Authorization:Bearer <access_token>
Параметры:
recipient - id пользователя адресата
message - сообщение

https://bitbucket.org/druos4/apis/raw/1f53062aea0863317523dc1e6cceb3b6bcae4119/public/images/sendmess.jpg


Получить сообщения

Url: http://localhost:8000/api/get-message
Method: post
Заголовки:
Accept:application/json
Authorization:Bearer <access_token>
Параметры:
sender - id пользователя отправителя

https://bitbucket.org/druos4/apis/raw/1f53062aea0863317523dc1e6cceb3b6bcae4119/public/images/getmess.jpg





