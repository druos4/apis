<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Message;
use Validator;

class MessageController extends BaseController
{

    public function sendMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'recipient' => 'required|exists:users,id',
            'message' => 'required|string|min:1',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $message = Message::sendMessage($request->all());

        return response()->json(['id' => $message->id], 200);
    }


    public function getMessage(Request $request)
    {
        $messages = Message::getMessagesFromUser($request->get('sender'));
        return response()->json(['messages' => $messages], 200);
    }

}
