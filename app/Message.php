<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'sender',
        'recipient',
        'room',
        'message',
        'viewed_at',
    ];


    public static function sendMessage($input)
    {
        $room = self::makeRoom($input['recipient']);
        $data = ['sender' => auth()->id(),
            'recipient' => $input['recipient'],
            'room' => $room,
            'message' => $input['message']];
        $message = Message::create($data);
        return $message;
    }


    public static function getMessagesFromUser($sender)
    {
        $room = self::makeRoom($sender);
        $messages = Message::where('room',$room)->where('sender',$sender)->orderBy('id','asc')->get();
        return $messages;
    }


    public static function makeRoom($target){
        //создаем идентификатор комнаты-диалога
        if($target < auth()->id()){
            $room = $target.'-'.auth()->id();
        } else {
            $room = auth()->id().'-'.$target;
        }
        return $room;
    }

}
